package com.bobby.week5_aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.bobby.week5_aboutme.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myName:MyName = MyName("Chattrawut")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.apply {
            done_button.setOnClickListener { v ->
                addNickname(v)
            }

            nickname_text.setOnClickListener {v->
                updateNickname(v)
            }
            this.myName = this@MainActivity.myName
        }
    }

    fun addNickname(v: View) {
        binding.apply {
            done_button.visibility = View.GONE
            nickname_edit.visibility = View.GONE
            myName?.nickname = nickname_edit.text.toString()
            nickname_text.visibility = View.VISIBLE
            invalidateAll()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
        }

    }

    private fun updateNickname(v: View) {
        binding.apply {
            nickname_edit.visibility = View.VISIBLE
            done_button.visibility = View.VISIBLE
            v.visibility = View.GONE
            nickname_edit.requestFocus()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(nickname_edit, 0)
        }

    }
}